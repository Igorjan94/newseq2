'use strict';

var _ = require('lodash')
  , Abstract = require('../abstract')
  , ConnectionManager = require('./connection-manager')
  , Query = require('./query')
  , DataTypes = require('./data-types')
  , QueryGenerator = require('./query-generator');

var TarantoolDialect = function(sequelize) {
  this.sequelize = sequelize;
  this.connectionManager = new ConnectionManager(this, sequelize);
  this.connectionManager.initPools();
  this.QueryGenerator = _.extend({}, QueryGenerator, {
    options: sequelize.options,
    _dialect: this,
    sequelize: sequelize
  });
};

ConnectionManager.prototype.defaultVersion = '5.6.0';
TarantoolDialect.prototype.Query = Query;
TarantoolDialect.prototype.QueryGenerator = QueryGenerator;
TarantoolDialect.prototype.name = 'tarantool';
TarantoolDialect.prototype.DataTypes = DataTypes;
TarantoolDialect.prototype.supports = _.merge(_.cloneDeep(Abstract.prototype.supports));

module.exports = TarantoolDialect;
