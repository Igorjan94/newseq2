'use strict';

var Utils = require('../../utils');

var QueryGenerator = {
  dialect: 'tarantool',
  query: function() {
    return arguments;
  },
  insertQuery: function(tableName, valueHash) {
    return ['insert', tableName, valueHash];
  },

  selectQuery: function(tableName, options, model) {
    let to = {
      where: options.where,
      scopes: options.includeNames || options.scopes,
      limit: options.limit,
      offset: options.offset,
      page: options.page,
      order: options.order,
      actorId: options.actorId,
      paranoid: options.paranoid,
      format: options.format,
      extract: options.extract,
      fold: options.fold
    };
    if (options.fields)
      to.attributes = options.fields;
    return [options.findOne ? 'get' : 'find', tableName, to];
  },

  updateQuery: function(tableName, attrValueHash) {
    return ['update', tableName, attrValueHash];
  },

  updateAttributesQuery: function(tableName, attrValueHash) {
    return ['updateAttributes', tableName, attrValueHash];
  },

  deleteQuery: function(tableName, where) {
    return ['delete', tableName, where];
  },

  createSchema: function() {
    return [];
  },

  showSchemasQuery: function() {
    return [];
  },

  versionQuery: function() {
    return [];
  },

  createTableQuery: function(tableName, attributes, options) {
    return [];
  },

  showTablesQuery: function() {
    return [];
  },

  addColumnQuery: function(table, key, dataType) {
    return [];
    var query = 'ALTER TABLE <%= table %> ADD <%= attribute %>;'
      , attribute = Utils._.template('<%= key %> <%= definition %>')({
        key: this.quoteIdentifier(key),
        definition: this.attributeToSQL(dataType, {
          context: 'addColumn',
          foreignKey: key
        })
      });

    return Utils._.template(query)({
      table: this.quoteTable(table),
      attribute: attribute
    });
  },

  removeColumnQuery: function(tableName, attributeName) {
    return [];
    var query = 'ALTER TABLE <%= tableName %> DROP <%= attributeName %>;';
    return Utils._.template(query)({
      tableName: this.quoteTable(tableName),
      attributeName: this.quoteIdentifier(attributeName)
    });
  },

  changeColumnQuery: function(tableName, attributes) {
    return [];
    var query = 'ALTER TABLE <%= tableName %> <%= query %>;';
    var attrString = [], constraintString = [];

    for (var attributeName in attributes) {
      var definition = attributes[attributeName];
      if (definition.match(/REFERENCES/)) {
        constraintString.push(Utils._.template('<%= fkName %> FOREIGN KEY (<%= attrName %>) <%= definition %>')({
          fkName: this.quoteIdentifier(attributeName + '_foreign_idx'),
          attrName: this.quoteIdentifier(attributeName),
          definition: definition.replace(/.+?(?=REFERENCES)/,'')
        }));
      } else {
        attrString.push(Utils._.template('`<%= attrName %>` `<%= attrName %>` <%= definition %>')({
          attrName: attributeName,
          definition: definition
        }));
      }
    }

    var finalQuery = '';
    if (attrString.length) {
      finalQuery += 'CHANGE ' + attrString.join(', ');
      finalQuery += constraintString.length ? ' ' : '';
    }
    if (constraintString.length) {
      finalQuery += 'ADD CONSTRAINT ' + constraintString.join(', ');
    }

    return Utils._.template(query)({
      tableName: this.quoteTable(tableName),
      query: finalQuery
    });
  },

  renameColumnQuery: function(tableName, attrBefore, attributes) {
    return [];
    var query = 'ALTER TABLE <%= tableName %> CHANGE <%= attributes %>;';
    var attrString = [];

    for (var attrName in attributes) {
      var definition = attributes[attrName];

      attrString.push(Utils._.template('`<%= before %>` `<%= after %>` <%= definition %>')({
        before: attrBefore,
        after: attrName,
        definition: definition
      }));
    }

    return Utils._.template(query)({ tableName: this.quoteTable(tableName), attributes: attrString.join(', ') });
  },

  showIndexesQuery: function(tableName, options) {
    return [];
    var sql = 'SHOW INDEX FROM <%= tableName %><%= options %>';
    return Utils._.template(sql)({
      tableName: this.quoteTable(tableName),
      options: (options || {}).database ? ' FROM `' + options.database + '`' : ''
    });
  },

  removeIndexQuery: function(tableName, indexNameOrAttributes) {
    return [];
    var sql = 'DROP INDEX <%= indexName %> ON <%= tableName %>'
      , indexName = indexNameOrAttributes;

    if (typeof indexName !== 'string') {
      indexName = Utils.inflection.underscore(tableName + '_' + indexNameOrAttributes.join('_'));
    }

    return Utils._.template(sql)({ tableName: this.quoteIdentifiers(tableName), indexName: indexName });
  },

  attributeToSQL: function(attribute, options) {
    return [];
    if (!Utils._.isPlainObject(attribute)) {
      attribute = {
        type: attribute
      };
    }

    var template = attribute.type.toString({ escape: this.escape.bind(this) });

    if (attribute.allowNull === false) {
      template += ' NOT NULL';
    }

    if (attribute.autoIncrement) {
      template += ' auto_increment';
    }

    // Blobs/texts cannot have a defaultValue
    if (attribute.type !== 'TEXT' && attribute.type._binary !== true && Utils.defaultValueSchemable(attribute.defaultValue)) {
      template += ' DEFAULT ' + this.escape(attribute.defaultValue);
    }

    if (attribute.unique === true) {
      template += ' UNIQUE';
    }

    if (attribute.primaryKey) {
      template += ' PRIMARY KEY';
    }

    if (attribute.after) {
      template += ' AFTER ' + this.quoteIdentifier(attribute.after);
    }

    if (attribute.references) {
      attribute = Utils.formatReferences(attribute);

      if (options && options.context === 'addColumn' && options.foreignKey) {
        var attrName = options.foreignKey;

        template += Utils._.template(', ADD CONSTRAINT <%= fkName %> FOREIGN KEY (<%= attrName %>)')({
          fkName: this.quoteIdentifier(attrName + '_foreign_idx'),
          attrName: this.quoteIdentifier(attrName)
        });
      }

      template += ' REFERENCES ' + this.quoteTable(attribute.references.model);

      if (attribute.references.key) {
        template += ' (' + this.quoteIdentifier(attribute.references.key) + ')';
      } else {
        template += ' (' + this.quoteIdentifier('id') + ')';
      }

      if (attribute.onDelete) {
        template += ' ON DELETE ' + attribute.onDelete.toUpperCase();
      }

      if (attribute.onUpdate) {
        template += ' ON UPDATE ' + attribute.onUpdate.toUpperCase();
      }
    }

    return template;
  },

  attributesToSQL: function(attributes, options) {
    return attributes;
    var result = {}
      , key
      , attribute;

    for (key in attributes) {
      attribute = attributes[key];
      result[attribute.field || key] = this.attributeToSQL(attribute, options);
    }

    return result;
  },

  findAutoIncrementField: function(factory) {
    return [];
    var fields = [];

    for (var name in factory.attributes) {
      if (factory.attributes.hasOwnProperty(name)) {
        var definition = factory.attributes[name];

        if (definition && definition.autoIncrement) {
          fields.push(name);
        }
      }
    }

    return fields;
  },

  quoteIdentifier: function(identifier) {
    return [];
    if (identifier === '*') return identifier;
    return Utils.addTicks(identifier, '`');
  },

  /**
   * Generates an SQL query that returns all foreign keys of a table.
   *
   * @param  {String} tableName  The name of the table.
   * @param  {String} schemaName The name of the schema.
   * @return {String}            The generated sql query.
   */
  getForeignKeysQuery: function(tableName, schemaName) {
    return [];
    return "SELECT CONSTRAINT_NAME as constraint_name FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE where TABLE_NAME = '" + tableName + /* jshint ignore: line */
      "' AND CONSTRAINT_NAME!='PRIMARY' AND CONSTRAINT_SCHEMA='" + schemaName + "' AND REFERENCED_TABLE_NAME IS NOT NULL;"; /* jshint ignore: line */
  },

  /**
   * Generates an SQL query that returns the foreign key constraint of a given column.
   *
   * @param  {String} tableName  The name of the table.
   * @param  {String} columnName The name of the column.
   * @return {String}            The generated sql query.
   */
  getForeignKeyQuery: function(table, columnName) {
    return [];
    var tableName = table.tableName || table;
    if (table.schema) {
        tableName = table.schema + '.' + tableName;
    }
    return [
      'SELECT CONSTRAINT_NAME as constraint_name',
      'FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE',
      'WHERE (REFERENCED_TABLE_NAME = ' + wrapSingleQuote(tableName),
        'AND REFERENCED_COLUMN_NAME = ' + wrapSingleQuote(columnName),
        ') OR (TABLE_NAME = ' + wrapSingleQuote(tableName),
        'AND COLUMN_NAME = ' + wrapSingleQuote(columnName),
        ')',
    ].join(' ');
  },

  /**
   * Generates an SQL query that removes a foreign key from a table.
   *
   * @param  {String} tableName  The name of the table.
   * @param  {String} foreignKey The name of the foreign key constraint.
   * @return {String}            The generated sql query.
   */
  dropForeignKeyQuery: function(tableName, foreignKey) {
    return 'ALTER TABLE ' + this.quoteTable(tableName) + ' DROP FOREIGN KEY ' + this.quoteIdentifier(foreignKey) + ';';
  }
};

// private methods
function wrapSingleQuote(identifier){
  return Utils.addTicks(identifier, '\'');
}

module.exports = Utils._.extend(Utils._.clone(require('../abstract/query-generator')), QueryGenerator);
