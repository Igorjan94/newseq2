'use strict';

var AbstractConnectionManager = require('../abstract/connection-manager')
  , ConnectionManager
  , Utils = require('../../utils')
  , Promise = require('../../promise')
  , sequelizeErrors = require('../../errors')
  , Tarantool = require('tarantool-driver')
  , dataTypes = require('../../data-types').mysql
  , parserMap = {};

ConnectionManager = function(dialect, sequelize) {
  AbstractConnectionManager.call(this, dialect, sequelize);

  this.sequelize = sequelize;
  this.sequelize.config.port = this.sequelize.config.port || 3301;
  this.refreshTypeParser(dataTypes);
};

Utils._.extend(ConnectionManager.prototype, AbstractConnectionManager.prototype);

ConnectionManager.prototype.connect = function(config) {
  var self = this;
  return new Promise(async function (resolve, reject) {
    var connectionConfig = {
      host: config.host,
      port: config.port,
      username: config.username,
      password: config.password,
      database: config.database,
      timezone: self.sequelize.options.timezone,
      retryStrategy: times => console.log(`Reconnecting... ${times}-th time`) || Math.min(30000, times * 1000)
    };

    if (config.dialectOptions) {
      Object.keys(config.dialectOptions).forEach(function(key) {
        connectionConfig[key] = config.dialectOptions[key];
      });
    }

    var connection = new Tarantool(connectionConfig);
    resolve(connection);
  });
};

ConnectionManager.prototype.disconnect = function(connection) {

  // Dont disconnect connections with an ended protocol
  // That wil trigger a connection error
  return connection.ping();

  return new Promise(function (resolve, reject) {
    connection.end(function(err) {
      if (err) return reject(new sequelizeErrors.ConnectionError(err));
      resolve();
    });
  });
};
ConnectionManager.prototype.validate = function(connection) {
  return connection && ['disconnected', 'protocol_error'].indexOf(connection.state) === -1;
};

module.exports = ConnectionManager;